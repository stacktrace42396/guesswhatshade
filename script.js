var numSquares = 6;
var colors = [];
var pickedColor;

var h1 = document.querySelector("h1");
var resetButton = document.getElementById("reset");
var squares = document.querySelectorAll(".square");
var modeButtons = document.querySelectorAll(".mode");
var messageDisplay = document.getElementById("message");
var colorDisplay = document.getElementById("colorDisplay");

initialize();

function initialize() {
  initModeButtons();
  initSquares();
  reset();
}

function initModeButtons() {
  modeButtons.forEach(function(mode) {
    mode.addEventListener("click", function() {
      modeButtons[0].classList.remove("selected");
      modeButtons[1].classList.remove("selected");
      this.classList.add("selected");
      numSquares = (this.textContent === "Easy") ? 3 : 6;
      reset();
    });
  });
}

function initSquares() {
  squares.forEach(function(square, index) {
    square.addEventListener("click", function() {
      var clickedColor = this.style.backgroundColor;
      if (clickedColor === pickedColor) {
        messageDisplay.textContent = "Correct!";
        resetButton.textContent = "Play Again?";
        changeColors(clickedColor);
        h1.style.backgroundColor = clickedColor;
      } else {
        this.style.backgroundColor = "#232323";
        messageDisplay.textContent = "Try Again";
      }
    });
  });
}

function reset() {
  colors = generateRandomColors(numSquares);
  pickedColor = pickColor();
  colorDisplay.textContent = pickedColor;
  resetButton.textContent = "New Colors";
  messageDisplay.textContent = "";
  
  squares.forEach(function(square, index) {
    if (colors[index]) {
      square.style.display = "block";
      square.style.backgroundColor = colors[index];
    } else {
      square.style.display = "none";
    }
  });

  h1.style.backgroundColor = "slateblue";
}

resetButton.addEventListener("click", function() {
  reset();
});

function pickColor() {
  var random = Math.floor(Math.random() * colors.length);
  return colors[random];
}

function changeColors(color) {
  squares.forEach(function(square) {
    square.style.backgroundColor = color;
  });
}

function generateRandomColors(num) {
  var arr = [];
  for (var i = 0; i < num; i++) {
    arr.push(randomColor());
  }
  return arr;
}

function randomColor() {
  var red = Math.floor(Math.random() * 256);
  var green = Math.floor(Math.random() * 256);
  var blue = Math.floor(Math.random() * 256);
  return "rgb(" + red + ", " + green + ", " + blue + ")";
}